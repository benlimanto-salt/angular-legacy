(function() {
    'use strict';
    // ngComponentRouter is needed for component routing!
    let app = angular.module('App', ['ngComponentRouter']);
    app.config(function($locationProvider) {
        $locationProvider.html5Mode(true);
    });
    
    app.controller('AppController', AppController);

    app.controller('ListController', ListController);

    AppController.$inject = [];
    function AppController() {
        var vm = this;
        
        vm.click = () => {
            alert("Angular 1");
        };

        activate();

        ////////////////

        function activate() { }
    }

    function ListController($scope) {
        var lvm = this;
        lvm.text = "Aaaa";
        lvm.data = ['a', 'b', 'c', 'd', 'e'];
        // console.log($scope);
        activate();

        function activate() {}
    }

    /**
     * @type angular.IComponentOptions
     */
    let botakText = {
        template: "Botak {{$ctrl.name}}",
        bindings: {
            name: "<"
        },
        controller: function BotakTextComponent() {
            // this.name = "BOTAK";
            // console.log($scope);
        }
    };

    app.component("botakText", botakText);

    // Example Routing Element... 
    // @see https://docs.angularjs.org/guide/component-router#example-heroes-app
    let punchButton = {
        template: `<button ng-click="$ctrl.click()">Punch Me!</button>`,
        controller: function PunchButtonComponent() 
        {
            var vm = this;

            vm.click = () =>
            {
                alert("Hai");
            }
        }
    };

    app.component("punchButton", punchButton);

    let appRoute = {
        template: `
            <nav>
                <a ng-link="['Hai']">Hai</a>
                <a ng-link="['Halo']">Halo</a>
            </nav>
            <ng-outlet></ng-outlet>
        `,
        $routeConfig: [
            { path: '/hai', name: 'Hai', component: 'punchButton', useAsDefault: true },
            { path: '/halo', name: 'Halo', component: 'botakText' },
        ]
    };

    app.value('$routerRootComponent', 'appRoute').component("appRoute", appRoute);
})();