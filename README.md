# Angular 1 Research - SALT.id Malang - How to Use AngularJS in 2023

> Be ware this is a internal document that shared publicly, we have a air-gap production deployment software that stil require angularJS. Please by the grace of GOD, don't deploy new angularJS on non air-gap production deployment in 2023! 

This is a research for angular 1 / angularJS (the legacy angular), corporate still have it in production, but most tuts are lost... 

The only way to learn it is via the angularjs website, but without any auto complete, it's sad... 

So I found a resource that still works... (some implementation in Typescript are my own tinkering and ways, as babel isn't officially supported by angularJS at that time. I use babel because it's feature rich, simpler, and can be included in the project, and most people are using NPM these days... so please don't judge me for my __opinionated__ solution. Thank you)

Read More : 
[Web Archive](http://web.archive.org/web/20230530215052/https://www.developerhandbook.com/blog/typescript/writing-angularjs-1-x-with-typescript/) | [Archive IS](https://archive.is/qwcjz) | [Original Source](https://www.developerhandbook.com/blog/typescript/writing-angularjs-1-x-with-typescript/) | [AngularJS RIP Tuts PDF](https://riptutorial.com/Download/angularjs.pdf) | [Developer Docs](https://docs.angularjs.org/guide#templates)

## JS Autocomplete 

To have JS Autocomplete, we need to make the project into node project using 

```shell
npm init project -y
```

Then we can install the angular 1 types 

```shell
npm i @types/angular
npm i @types/angular-route
npm i @types/angular-cookies
```

Then you will have the autocomplete in any .js or .ts files using VS Code 

## Run Project

In order the route to work, you need a simple web server to serve the project, we can use nginx

```shell
podman run -it --rm -v .:/usr/share/nginx/html:ro -p 8000:80 nginx
```

I'm using podman, if you use docker, just replace podman command to docker command. But podman is much more faster than docker for development.. 

And if you don't want it to be removed when ctrl+c, then remove the `--rm`, just use `-d` flags

### Building from .ts to .js using Babel 

Problem with .js is there won't be really powerful strict type like .ts, so we can employ babel to do the works. To use babel, we need to install babel and it's presets

```
npm install @babel/core @babel/cli @babel/preset-typescript @babel/preset-env
```

Then define the file config of babel, refer to https://babeljs.io/docs/options#output-targets

For presets needs, see more at https://github.com/babel/babel/discussions/16050

```json
{
    "presets": [
        [
            "@babel/preset-env",
            {
                "targets": {
                    "chrome": "58",
                    "ie": "11"
                },
                // for uglifyjs...
                // @see https://babeljs.io/docs/babel-preset-env#forcealltransforms
                "forceAllTransforms": true
            }
        ]
    ],
    // @see https://babeljs.io/docs/options#ignore
    "ignore": [
        "src/interfaces/**"
    ]
}
```

then we can run the babel watch and builder with some command (and ignore some interface folder we don't need, [see more](https://github.com/babel/babel/issues/6226#issuecomment-590283042))


```
npx babel --watch src --out-dir assets/js --extensions .ts --presets=@babel/preset-typescript,@babel/preset-env --source-maps inline --copy-files --no-copy-ignored
```

we can move that command to npm scripts in the package.json, if you need it. If not then well, just run one time when you need it, and kill it (ctrl+c) when you don't

`--copy-files` should be added as a way to copy .html template so we don't need to copy the files manually see more explanation on [babel copy docs](https://babeljs.io/docs/babel-cli#copy-files) and [SO answer](https://stackoverflow.com/a/35033177/4906348)

#### Component Template Location 

For now, to get the template location, we can't directly `./path-to.html`, but using `App.config.templatePath` on the `templateUrl`, eg.

```typescript
let component: angular.IComponentOptions = {
    templateUrl: `${App.config.templatePath}/punch-button-component/punchButton.html`,
    controller: PunchButtonComponent
};

app.component("punchButton", component);
```

### How to include component/modules

You must manually include the file into the `index.html` or any `.html` files that need it, you can put it before end of body, for now the structure is 

```
assets/
├── css
│   └── main.css
└── js
    ├── app.js
    ├── components
    │   ├── app-route-component
    │   │   ├── app-route.html
    │   │   └── app-route.js
    │   ├── botak-text-component
    │   │   ├── botakText.html
    │   │   └── botakText.js
    │   ├── home-component
    │   │   ├── home.html
    │   │   └── home.js
    │   └── punch-button-component
    │       ├── punchButton.html
    │       └── punchButton.js
    ├── config.js
    └── services
        └── tasks.service.js
```

if example you need the `app-route` component or others, then your need to include as example :

```html
<!-- ommitted, see index.html -->
    <script src="assets/js/app.js"></script>
    <script src="assets/js/components/punch-button-component/punchButton.js"></script>
    <script src="assets/js/components/botak-text-component/botakText.js"></script>
    <script src="assets/js/components/home-component/home.js"></script>
    <script src="assets/js/components/app-route-component/app-route.js"></script>
<!-- ommitted -->
```

### Lazy Loading - Built in - How?

To be honest, this lazy loading is quite headache for me, because there are a lot of hack on the code, to make it works. The base concept you can check is by reading [this implementation](https://stackoverflow.com/a/32061320/4906348) and [further explanation](https://stackoverflow.com/a/28696096/4906348).

TDLR; We are using cache mode on window by attaching the controller and other provider function, and use it on the contoller that we lazy load using typescript. This only work for controller, not component. ~~For component we need to find workaround.~~ From the angular-seed (angular1 project, [see](https://github.com/angular/angular-seed)), seems it's quite impossible, as they have different approach from angular 2, so the only option is... well... sadly.. the only way is declare any component before the lazy load page... this also applied to deprecated `ngComponentRouter`, either we use custom promise lazyLoader, or... divide it into multiple html... sadly.. even the DanWahlin AngularJS Project using MVC5 [see the app.js](https://github.com/DanWahlin/CustomerManager/blob/main/CustomerManager/app/app.js), [index.html](https://github.com/DanWahlin/CustomerManager/blob/main/CustomerManager/index.html#L93), and [requireJS with angularJS](https://stackoverflow.com/questions/12529083/does-it-make-sense-to-use-require-js-with-angular-js/20954143#20954143), only achieve what this code in this repository able to (in short, same, but different approad, no dependency other than angularJS custom service), so well.. only controller able to be lazyLoaded, and no component could be lazy loaded (from different module).

To see the implementation, read these code inside `src`
See more on the files `app-route.ts`, `components/task-add/task-add.ts`, and `components/task-list/task-list.ts`

### Interceptors (HTTP)

We need to use interceptors for adding authorization, or others process before and after the request. At the interceptor, to use the service that require `$http`, we must use monkey DI/`inject()` function to able by pass the $http configured before using our interceptor, even the service use angular service. So if you need a service and want to use serviceProviders on the config, always check does the service require using `$http`? If yes, then don't use the serviceProviders, but use `inject()`. 

for more, see `interceptors/auth.interceptor.ts`.

# Unit Testing

We won't use karma (these day on 2023...), and karma project is deprecated. We can use [jest](https://jestjs.io/docs/getting-started).

## Jest approach

Installing the package 

```shell
# part of Jest Test Tools for JS and Import HTML
npm install --save-dev jest jest-environment-jsdom @types/jest jest-html-loader html-loader
npm install --save-dev babel-jest @babel/core @babel/preset-env @babel/preset-typescript
# part of only angular mock tools
npm install --save-dev angular angular-mocks angular-route angular-cookies @types/angular-mocks 
```

Set the config for the jest on `jest.config.json`, readmore [config](https://jestjs.io/docs/configuration)
```json
{
    "transform": {
        "^.+\\.ts$": "babel-jest",
        // for Jest HTML Loader, @see https://stackoverflow.com/a/41558029/4906348
        // and @see https://github.com/webpack-contrib/html-loader, @see https://github.com/nicoespeon/jest-html-loader/tree/main
        "^.+\\.html?$": "jest-html-loader"
    },
    "testEnvironment": "jsdom",
    "collectCoverage": true,
    "coverageDirectory": "coverage",
    "coverageProvider": "babel
}
```

After creating test File, we need to check, what kind of code we want to test, is it controller, component, service, directive, or other things. 

For controller, we need to inject the `$controller`, and create one as example 

```typescript
let component: RouteApp.LoginPageController;
const module = angular.mock.module;
const inject = angular.mock.inject;
let q: angular.IQService;
let rootScope: angular.IRootScopeService;

var templateHtml, formElem, form;

beforeEach(() => {
    var ctrl: angular.IControllerService, scope: angular.IRootScopeService|any;
    module('RouteApp'); // The name of the module, make sure we already require/import it first
    // To inject the controller and other, we need to do these things... 
    // $scope = https://stackoverflow.com/a/43274428/4906348
    // $controller = https://stackoverflow.com/a/59054213/4906348
    inject(function($rootScope: angular.IRootScopeService, $controller: angular.IControllerService, $templateCache: angular.ITemplateCacheService, 
            $compile: angular.ICompileService, $q: angular.IQService) {
        // console.log(JSON.parse(_$controller_));
        ctrl = $controller;
        rootScope = $rootScope;
        q = $q;
        scope = $rootScope.$new();
        // The HTML example, without importing. Still on the way.. 
        templateHtml = require("./login.component.html");
        formElem = angular.element(`<div>${templateHtml}</div>`);
        $compile(formElem)(scope)
        form = scope.form;

        scope.$apply()
        // console.log("huh",templateHtml);
    });

    component = ctrl('loginPage', {$scope: scope});
});
```

for component, it's a little bit different


```typescript
let component: App.PunchButtonComponent;
const module = angular.mock.module;
const inject = angular.mock.inject;
const spyOn = jest.spyOn;
var el;
let rootScope: angular.IRootScopeService;

beforeEach(() => {
    var cmpnt: angular.IComponentControllerService, scope: angular.IRootScopeService|any;
    module('PunchModule');
    // To inject the controller and other, we need to do these things... 
    // $scope = https://stackoverflow.com/a/43274428/4906348
    // $controller = https://stackoverflow.com/a/59054213/4906348
    // $componentController https://docs.angularjs.org/api/ngMock/service/$componentController#!
    inject(function($rootScope: angular.IRootScopeService, $componentController: angular.IComponentControllerService, $templateCache: angular.ITemplateCacheService, 
            $compile: angular.ICompileService, $httpBackend: angular.IHttpBackendService) {
        // console.log(JSON.parse(_$controller_));
        // This is used because we need to give responds fro HTML template of the component
        $httpBackend.whenGET(/\/*/).respond(require('./punchButton.html'));

        cmpnt = $componentController;
        rootScope = $rootScope;

        scope = $rootScope.$new();

        el = angular.element(`<punch-button></punch-button>`);
        $compile(el)(scope)
        // console.log(el[0]);
        // form = scope.form;

        scope.$apply();
        // console.log("huh",templateHtml);
    });

    component = cmpnt('punchButton', {$scope: scope});
});
```