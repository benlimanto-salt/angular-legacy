namespace RouteApp
{
    // We need to change this module depends on what module we want to.. 
    let ng = angular.module("RouteApp");

    export class LazyService implements angular.IServiceProvider {
        public promisesCache = {};
        // static $inject = ['$http'];
        private static singleton: LazyService;
        private http: angular.IHttpService;
        constructor() {
            this.http = angular.injector(['ng']).get("$http");
            // console.log(this.http);
        }

        public loadComponent(name: string, path: string = '') {
            path = (path == name) ? name : path;

            var path = `components/${path}/${name}.component.js`;

            let promise = this.loadScript(`${path}`);
            
            return promise;
        }

        public loadScript(path: string) {
            path = `${config.jsPath}/${path}`;
            var promise: angular.IHttpPromise<any>|null = this.promisesCache[path];
            if (!promise) {
                var promise = this.http.get<any>(path);
                this.promisesCache[path] = promise;

                return promise.then(function(result) {
                    // Not sure if this is really secure...
                    // But RequireJS done the same... 
                    eval(result.data);
                    console.log('Loaded: ' + path);
                });
            }
            
            return promise;
        }

        public loadService(name: string) {
            return this.loadScript(`services/${name}.service.js`);
        }

        public static Singleton()
        {
            if (LazyService.singleton == null) LazyService.singleton = new LazyService();

            return LazyService.singleton;
        }

        public resolve(controllerName: string, controllerFileName: string,services: Array<string> = [], customRouteProperties: any = {},controllerPath: string = '', controllerAlias: string = 'vm')
        {
            controllerPath = (controllerPath == '') ? controllerFileName : controllerPath;

            var resolveObj = {
                ctrl: ['lazyService', (srv: LazyService) => {
                    return srv.loadComponent(controllerFileName, controllerPath);
                }]
            };

            services.forEach(s => {
                resolveObj[s] = ['lazyService', (srv: LazyService) => {
                    return srv.loadService(s);
                }]
            });

            var res = {
                templateUrl: `${config.templatePath}/${controllerPath}/${controllerFileName}.component.html`,
                controller: `${controllerName} as ${controllerAlias}`,
                resolve: resolveObj
            };

            return { ...res, ...customRouteProperties}; // Merge custom Properties
        }

        $get() {
            return LazyService.Singleton();
        };
    }

    // Need a factory workaround :/ 
    // This is sad... we need to declare it... outside not like inside.. and for factory, should return class new.. 
    // LazyService.Singleton.$inject = ['$http'];

    // console.log("Lazy : ", LazyService.Factory);
    
    ng.provider("lazyServiceProvider", LazyService);

    ng.factory('lazyService', LazyService.Singleton);
}