namespace RouteApp {
    'use strict';
    
    export interface Task 
    {
        userId: number;
        id: number;
        title: string;
        completed: boolean;    
    }

    export interface ITaskService {
        getList(page?: number): angular.IHttpPromise<Array<Task>>
        get(id): angular.IHttpPromise<Task>
        update(task: Task): angular.IHttpPromise<Task>
        insert(task: Task) :  angular.IHttpPromise<Task>
    }

    class TaskService implements ITaskService {
        constructor(private http: angular.IHttpService) {}

        update(task: Task): ng.IHttpPromise<Task> {
            return this.http.patch<Task>(`${config.apiURL}/todos/${task.id}`, task);
        }

        get(id: any): ng.IHttpPromise<Task> {
            return this.http.get<Task>(`${config.apiURL}/todos/${id}`);
        }
        
        public insert(task: Task): ng.IHttpPromise<any> {
            return this.http.post<Array<Task>>(`${config.apiURL}/todos`, task);
        }

        public getList(page: number = 1)
        {
            return this.http.get<Array<Task>>(`${config.apiURL}/todos?_page=${page}&_limit=6`);
        }

        public static Factory(http: angular.IHttpService)
        {
            return new TaskService(http);
        }
    }

    // This is sad... we need to declare it... outside not like inside.. and for factory, should return class new.. 
    TaskService.Factory.$inject = [`$http`];
    
    // console.log(TaskService.Factory);

    let ng = angular
        .module('RouteApp').lazy;
    
    // Register the Service using same Module 
    // @see https://docs.angularjs.org/guide/services#creating-services
    // @see https://devtut.github.io/angularjs/distinguishing-service-vs-factory.html#factory-vs-service-once-and-for-all
    let srv = ng.factory('TaskService', TaskService.Factory);
    // console.log(srv);
}