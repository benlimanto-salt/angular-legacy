// @see 
// declare jasmine, ugh.. 
// angular-mocks only available for jasmine or mocha.. zzzz
// Old tech.. sad...
// This can be seen form the angular-mocks.js in node_modules, This is my own workaround
(window.jasmine as any) = jest;

// require is like, well.. 

require("angular");
require("angular-route");
require("angular-cookies");
require('angular-mocks/ngMockE2E');
require("jest");

// Other Controller Dependency of component
require('../config');
require('./auth.service');

describe("Auth Service Test", () => {
    let service: RouteApp.IAuthService;
    const module = angular.mock.module;
    const inject = angular.mock.inject;
    let rootScope: angular.IRootScopeService;

    beforeEach(() => {
        // Call the mock e2e
        module("AuthServiceModule", "ngMockE2E");
        inject(function($httpBackend: angular.IHttpBackendService, $rootScope: angular.IRootScopeService) {
            $httpBackend.whenPOST(/\/*/).passThrough();
            $httpBackend.whenGET(/\/*/).passThrough();
            rootScope = $rootScope;

            service = angular.injector(['AuthServiceModule']).get('AuthService');
        });
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should be able to login', async () => {
        rootScope.$digest();

        return service.login("ben", "ben").then(d => {
            console.log(d.data);
            expect((d.data as any).id).toBeGreaterThan(0);
            return d;
        });
    }, 10000);

    it('should be able to logout', async () => {
        rootScope.$digest();

        return service.logout().then(d => {
            console.log(d.data);
            expect((d.data as any).id).toBeGreaterThan(0);
            return d;
        });
    }, 10000);
});