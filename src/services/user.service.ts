namespace RouteApp
{
    let ng = angular.module("RouteApp").lazy;

    export interface User 
    {
        id: number,
        name: string,
        username: string,
        email: string,
        phone: string,
        website: string
    }

    export interface IUserService
    {
        getList(page?: number): angular.IHttpPromise<User[]>
    }


    class UserService implements IUserService
    {
        static $inject = ['$http'];

        constructor(private http: angular.IHttpService)
        {

        }

        getList(page: number = 1)
        {
            return this.http.get<User[]>(`${config.apiURL}/users?_limit=5&_page=${page}`);
        }


    }

    ng.service("UserService", UserService);
}