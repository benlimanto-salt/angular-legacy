(window as any).config = {
    jsPath: `/assets/js`,
    templatePath: `/assets/js/components`,
    apiURL: `https://jsonplaceholder.typicode.com`
}

const config = (window as any).config;