namespace RouteApp
{
    // I prefer use the config rather than provider service
    // @see https://www.c-sharpcorner.com/UploadFile/dev4634/understanding-http-interceptors-in-angular-js/
    // @see https://docs.angularjs.org/api/ng/service/$http#interceptors
    // Why we don't use lazy function? Because if we don't pass array module on definition, we will have same module/get the latest module 
    // that's already defined... and this is the only answer.. 

    let ng = angular.module("RouteApp");
    export const BearerInterceptor: angular.IHttpInterceptor = {
        request: (config) => {
            // In the end, Monkey DI is the answer for config... 
            // Because if we use ServiceProvider, it will lead to http initiated before the intercepter ever registered...
            // read https://docs.angularjs.org/api/auto/service/$injector#!
            let authSrv = angular.injector(['AuthServiceModule']).get<RouteApp.IAuthService>('AuthService');
            console.log(authSrv.isLogedIn());
            if (authSrv.isLogedIn())
            {
                // To Check if the path need authorization
                // localStorage.getItem("token");
                config.headers['Authorization'] = 'Bearer randomstring';
            }
            return config;
        },
        responseError: (reject: angular.IHttpResponse<any>) => {
            console.log("Error : ",reject);
            console.log(reject.data);
            return reject;
        }
    };

    function BearerFn(httpProvider: angular.IHttpProvider) {
        // let auth = authProv.$get();
        // console.log(auth);
        // Welp... WELP... Sad...
        httpProvider.interceptors.push(() => BearerInterceptor);
        // console.log(httpProvider);
    }
    
    BearerFn.$inject = ['$httpProvider', 'AuthServiceProvider'];

    // This config only enabled if... it's declared on top module/first loaded module.. 
    ng.config(BearerFn);
}