namespace App 
{
    let app = angular.module('BotakTextModule', []);

    export class BotakTextComponent {
        constructor(private $scope)
        {
            console.log(this.$scope);
        }

        public getName()
        {
            // console.log(this);
            return this['name'] ?? "Halo"; 
        }
    }

    let component: angular.IComponentOptions = {
        bindings: {
            name: "<"
        },
        controller: BotakTextComponent,
        templateUrl: `${config.templatePath}/botak-text/botak-text.component.html`
    }

    app.component("botakText", component);
}