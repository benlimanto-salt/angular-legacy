// @see 
// declare jasmine, ugh.. 
// angular-mocks only available for jasmine or mocha.. zzzz
// Old tech.. sad...
// This can be seen form the angular-mocks.js in node_modules, This is my own workaround
(window.jasmine as any) = jest;
window.alert = (str) => {console.log(str)}
export {}; // For able to declare same variable on different files https://stackoverflow.com/a/62912102/4906348

// require is like, well.. 

require("angular");
require("angular-route");
require("angular-cookies");
require('angular-mocks/ngMockE2E');

require('../../config');
require('../../services/auth.service');

require('./botak-text.component');
let htmlTemplate = require("./botak-text.component.html");

describe("Botak Text Component", () => {
    let component: App.BotakTextComponent;
    let scope: angular.IRootScopeService|any;
    const module = angular.mock.module;
    const inject = angular.mock.inject;
    const spyOn = jest.spyOn;
    var el;
    let rootScope: angular.IRootScopeService;

    var cmpnt: angular.IComponentControllerService;

    beforeEach(() => {
        module('BotakTextModule');
        // To inject the controller and other, we need to do these things... 
        // $scope = https://stackoverflow.com/a/43274428/4906348
        // $controller = https://stackoverflow.com/a/59054213/4906348
        // $componentController https://docs.angularjs.org/api/ngMock/service/$componentController#!
        inject(function($rootScope: angular.IRootScopeService, $componentController: angular.IComponentControllerService, $templateCache: angular.ITemplateCacheService, 
                $compile: angular.ICompileService, $httpBackend: angular.IHttpBackendService) {
            // console.log(JSON.parse(_$controller_));

            $httpBackend.whenGET(/\/*/).respond(htmlTemplate);

            cmpnt = $componentController;
            rootScope = $rootScope;

            scope = $rootScope.$new();

            el = angular.element(`<botak-text></botak-text>`);
            $compile(el)(scope)
            // console.log(el[0]);
            // form = scope.form;

            scope.$apply();
            console.log("huh",scope);
        });

        component = cmpnt('botakText', {$scope: scope}, {
            name: "Benyamin"
        });
    });

    it("should be created", () => {
        expect(component).toBeTruthy();
    });

    it("should have value Benyamin", () =>{
        expect(component.getName()).toEqual("Benyamin");
    });

    it("Should have default HALO", () => {
        component = cmpnt('botakText', {$scope: scope});
        expect(component.getName()).toEqual("Halo");
    });
});