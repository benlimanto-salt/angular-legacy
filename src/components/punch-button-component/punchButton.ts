namespace App 
{
    'use strict'

    let app = angular.module('PunchModule', []);

    export class PunchButtonComponent {
        public text: string;
        constructor()
        {
            this.text = "Ini Text";
        }

        public click()
        {
            alert(this.text);
        }
    }

    let component: angular.IComponentOptions = {
        templateUrl: `${config.templatePath}/punch-button-component/punchButton.html`,
        controller: PunchButtonComponent
    };

    app.component("punchButton", component);
}