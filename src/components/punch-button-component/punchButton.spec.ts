// @see 
// declare jasmine, ugh.. 
// angular-mocks only available for jasmine or mocha.. zzzz
// Old tech.. sad...
// This can be seen form the angular-mocks.js in node_modules, This is my own workaround
(window.jasmine as any) = jest;
window.alert = (str) => { console.log(str) };
export {};
// require is like, well.. 

require("angular");
require("angular-route");
require("angular-cookies");
require('angular-mocks/ngMock');

require("../../config");
// (window as any).config = {
//     templatePath: ''
// };
require("./punchButton");
let htmlTemplate = require("./punchButton.html");

describe("PunchButton", () => {
    let component: App.PunchButtonComponent;
    const module = angular.mock.module;
    const inject = angular.mock.inject;
    const spyOn = jest.spyOn;
    var el;
    let rootScope: angular.IRootScopeService;

    beforeEach(() => {
        var cmpnt: angular.IComponentControllerService, scope: angular.IRootScopeService|any;
        module('PunchModule');
        // To inject the controller and other, we need to do these things... 
        // $scope = https://stackoverflow.com/a/43274428/4906348
        // $controller = https://stackoverflow.com/a/59054213/4906348
        // $componentController https://docs.angularjs.org/api/ngMock/service/$componentController#!
        inject(function($rootScope: angular.IRootScopeService, $componentController: angular.IComponentControllerService, $templateCache: angular.ITemplateCacheService, 
                $compile: angular.ICompileService, $httpBackend: angular.IHttpBackendService) {
            // console.log(JSON.parse(_$controller_));

            $httpBackend.whenGET(/\/*/).respond(htmlTemplate);

            cmpnt = $componentController;
            rootScope = $rootScope;

            scope = $rootScope.$new();

            el = angular.element(`<punch-button></punch-button>`);
            $compile(el)(scope)
            // console.log(el[0]);
            // form = scope.form;

            scope.$apply();
            // console.log("huh",templateHtml);
        });

        component = cmpnt('punchButton', {$scope: scope});
    });

    it("PunchButton should be created", () => {
        expect(component).toBeTruthy();
    });

    it("PunchButton can be clicked", () => {
        // In Angular Karma/Modern Test, it's different
        const spyOnAlert = spyOn(window, "alert");

        component.click();

        expect(spyOnAlert).toHaveBeenCalled();
    });
});