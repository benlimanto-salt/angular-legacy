/// <reference path="../../../node_modules/@types/angular/index.d.ts" />
/// <reference path="../../../node_modules/@types/angular/angular-component-router.d.ts" />

namespace App 
{
    let app = angular.module('AppRouteModule', ['ngComponentRouter','PunchModule', 'BotakTextModule', 'HomeComponentModule']);
    
    app.config(function($locationProvider) {
        $locationProvider.html5Mode(true);
    });

    class AppRouteComponent
    {

    }

    let component: angular.IComponentOptions = {
        templateUrl: `${App.config.templatePath}/app-route-component/app-route.html`,
        controller: AppRouteComponent,
        $routeConfig: [
            { path: '/', name: 'Home', component: 'home', useAsDefault: true },
            { path: '/hai', name: 'Hai', component: 'punchButton' },
            { path: '/halo', name: 'Halo', component: 'botakText' },
        ]
    };

    app.value('$routerRootComponent', 'appRoute').component("appRoute", component);
}