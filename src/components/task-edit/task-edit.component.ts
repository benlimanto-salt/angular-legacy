namespace RouteApp
{
    let ng = angular.module("RouteApp").lazy;

    class TaskEditController implements angular.IController
    {
        static $inject: Array<string> = ['TaskService', '$routeParams', '$scope', '$location'];
        public id: number = -1;

        public form = {
            name: ''
        }

        constructor(private taskSrv: RouteApp.ITaskService, params: angular.route.IRouteParamsService,
            private scope: angular.IScope, private loc: angular.ILocationService)
        {
            this.id = params['id'];
            this.getData();
        }

        public reset()
        {
            this.getData();
        }

        public getData()
        {
            this.taskSrv.get(this.id).then(d => {
                this.form.name = d.data.title
            });
        }

        public getForm()
        {
            return this.scope['form'] as angular.IFormController; 
        }

        public submit()
        {
            if (this.getForm().$invalid) return;
            this.taskSrv.update({
                id: this.id,
                title: this.form.name,
                completed: false,
                userId: 0
            }).then(d => {
                alert("Updater berhasil");
                console.log(this);
                this.loc.path("/"); // back to list, event it's not changed, because service only mock
            });
        }
    }

    ng.controller("taskEdit", TaskEditController);
}