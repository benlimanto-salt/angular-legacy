namespace App 
{
    let ng = angular.module("HomeComponentModule", []);

    class HomeComponent 
    {
        public name: string = "";

        public formGroup = {
            name: "",
            email: "",
            items: []
        };

        /**
         * WOWOWOWOWOWO
         * @param scope The Scope of the Component
         * @param http HTTP Service Injected
         */
        constructor(private scope: angular.IScope, private http: angular.IHttpService)
        {
            (this.http.get("https://jsonplaceholder.typicode.com/users/1") as Promise<any>)
            .then(d => {
                console.log(d);
                this.formGroup.name = d.data.name;
                this.formGroup.email = d.data.email;
            });
        }

        public submit()
        {
            var form = this.getForm();
            alert("See the console for more data");
            console.log(form.$valid);
            // console.log(this.getFormControl('email').$error);
            console.log(JSON.stringify(this.formGroup, true as any, 2));
        }

        public getForm()
        {
            return this.scope['formRegister'] as angular.IFormController;
        }

        public getFormControl(name: string)
        {
            return this.getForm().$getControls().filter(c => c.$name == name)[0];
        }

        /**
         * For dynamicly add and remove forms
         */
        public addItem()
        {
            this.formGroup.items.push({
                name: "",
                qty: 0,
                product: false,
                service: false
            });
        }


        public removeItem(index: number)
        {
            this.formGroup.items.splice(index, 1);
        }
    }

    // For sake convinient and good one, use injection!
    // @see https://docs.angularjs.org/tutorial/step_07#a-note-on-minification
    HomeComponent.$inject = ['$scope','$http'];

    let component: angular.IComponentOptions = {
        templateUrl: `${App.config.templatePath}/home-component/home.html`,
        controller: HomeComponent
    };

    ng.component("home", component);
}