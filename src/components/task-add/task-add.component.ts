namespace RouteApp {
    'use strict';

    // Last RESORT!
    let ng = angular.module("RouteApp").lazy;
    // let ng = angular.module("RouteApp");
    // let mod = ng.module;
    // mod.requires.push('BotakTextModule');

    class TaskAddController implements angular.IOnInit
    {
        public form = {
            name: '',
            description: ''
        };

        static $inject = ['$scope', 'TaskService'];

        constructor(private scope: angular.IScope, private taskService: ITaskService)
        {
            console.log(this.taskService);
        }

        $onInit(): void {
            console.log("called!");
            throw new Error("Test");
        }

        getForm()
        {
            return this.scope['taskAdd'] as angular.IFormController;
        }

        public submit()
        {
            if (this.getForm().$invalid == false)
            {
                let t: Task = {
                    id : 0,
                    completed: false,
                    title: this.form.name,
                    userId: Math.floor(Math.random() * 10)
                };

                this.taskService.insert(t).then(a => {
                    console.log(a);
                });
            }
        }
    }

    // let component: angular.IComponentOptions = {
    //     controller: TaskAddController,
    //     templateUrl: `${RouteApp.config.templatePath}/task-add/task-add.component.html`
    // };
    // console.log("Module :",mod);
    // ng.component("taskAdd", component);
    
    ng.controller("taskAdd", TaskAddController);
    // console.log(con);
}