/// <reference path="../../interfaces/lazy.interface.ts" />
namespace RouteApp 
{
    let ng = angular.module("RouteApp");
    let lz = ng.lazy;
    // console.log("Lazy : ", ng);

    export class TaskListController implements angular.IOnInit
    {
        static $inject: Array<string> = ['TaskService', '$location', 'AuthService']
        public list: Array<Task> = [];

        public page: number = 1;
        public end: boolean = false;
        constructor(private taskService: RouteApp.ITaskService, private loc: angular.ILocationService,
            public authSrv: RouteApp.IAuthService)
        {
            // Sad thing in routing, onInit doesn't called.. seems bug
            // We need to manually call it.. zzzzz
            this.$onInit();
        }

        $onInit(): void {
            console.log("Init Called");
            this.getData();
        }

        getData()
        {
            return this.taskService.getList(this.page).then(d => {
                this.list = d.data;
                // console.log(d.data);
                if (d.data.length < 5) {
                    this.end = true;
                    if (d.data.length == 0)
                    {
                        this.prev();
                    }
                }
                else this.end = false;
            });
        }

        click()
        {
            this.$onInit();
            alert("hai");
        }

        prev()
        {
            this.page--;

            return this.getData();
        }

        next()
        {
            this.page++;

            return this.getData();
        }

        edit(id: number)
        {
            this.loc.path(`/task/${id}`);
        }
    }

    lz.controller("taskList", TaskListController);
    // console.log("controller :", con);
    // console.log(component);
}