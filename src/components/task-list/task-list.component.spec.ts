// @see 
// declare jasmine, ugh.. 
// angular-mocks only available for jasmine or mocha.. zzzz
// Old tech.. sad...
// This can be seen form the angular-mocks.js in node_modules, This is my own workaround
(window.jasmine as any) = jest;
window.alert = (str) => {console.log(str)}
export {}; // For able to declare same variable on different files https://stackoverflow.com/a/62912102/4906348

// require is like, well.. 

require("angular");
require("angular-route");
require("angular-cookies");
require('angular-mocks/ngMockE2E');

require('../../config');
require('../../services/auth.service');

// For app-route
let prov = angular.module("RouteApp", ['AuthServiceModule']);
// for MockE2E, read more https://docs.angularjs.org/api/ngMockE2E/service/$httpBackend#!
// Workaround to lazy load for specific... welp... God Please help.. 
// haha... 
(prov as any).lazy = {
    controller: prov.controller,
    provider: prov.provider,
    factory: prov.factory,
    service: prov.service,
    config: prov.config,
    module: prov
}

require('../../services/tasks.service');

require("./task-list.component");
let htmlTemplate = require('./task-list.component.html');

describe("TaskList Test", () => {
    let component: RouteApp.TaskListController;
    const module = angular.mock.module;
    const inject = angular.mock.inject;
    const spyOn = jest.spyOn;
    let q: angular.IQService;
    let rootScope: angular.IRootScopeService;

    var el: JQLite;

    beforeEach(() => {
        var ctrl: angular.IControllerService, scope: angular.IScope;
        module("RouteApp", "ngMockE2E"); // Register the RouteApp to the angular.mock
        inject(function($controller: angular.IControllerService, $rootScope: angular.IRootScopeService, $compile: angular.ICompileService,
            $httpBackend: angular.IHttpBackendService) {
            // Map all needed function and object for test
            ctrl = $controller; scope = $rootScope.$new(); rootScope = $rootScope;
            el = angular.element(htmlTemplate); // create the element
            // console.log("HTML : ",htmlTemplate);

            // Allow the data to be passed directly to API
            // As example to the other than using mock
            $httpBackend.whenGET(/\/*/).passThrough();
            
            $compile(el)(scope);

            scope.$apply();
        });

        component = ctrl("taskList", {$scope: scope});
    });

    it("should be created", () => {
        expect(component).toBeTruthy();
    });

    it("should get data", async () => {
        const spyOnGetData = spyOn(component, "getData");
        
        rootScope.$apply(); // Call all API... if it's mock, if not then it's not

        // This is quite a nightmare... we need to give answer with promise.. 
        // Haiyah... waliao leh.. 
        return component.getData().then(d => {
            expect(spyOnGetData).toHaveBeenCalled();
            expect(component.list.length).toBeGreaterThan(0);
            console.log(component.list[0]);
        });
    });

    it("should show alert", () => {
        const spyOnAlert = spyOn(window, "alert");
        component.click();
        expect(spyOnAlert).toHaveBeenCalled();
    });

    it("should able to next", () => {
        rootScope.$apply();
        return component.next().then(d => {
            expect(component.page).toBeGreaterThan(1);
            expect(component.list.length).toBeGreaterThan(0);
            console.log(component.list[0]);
        });
    });

    it("should able to prev", () => {
        rootScope.$apply();
        return component.prev().then(d => {
            expect(component.page).toBeLessThan(1);
            expect(component.list.length).toBeGreaterThan(0);
            console.log(component.list[0]);
        });
    });
});