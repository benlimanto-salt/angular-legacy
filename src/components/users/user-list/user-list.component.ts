namespace RouteApp
{
    let ng = angular.module("RouteApp").lazy;

    class UserListController implements angular.IController, angular.IOnInit
    {
        public data = [];
        private page = 1;
        public end = false;
        static $inject = ['UserService'];

        constructor(private userSrv: RouteApp.IUserService)
        {
            // on this condition of lazy load, on init need to be called manually
            this.$onInit();
        }

        $onInit(): void {
            this.getData();
        }

        getData() 
        {
            this.userSrv.getList(this.page).then(d => {
                this.data = d.data;

                if (d.data.length < 5) {
                    this.end = true;
                    if (d.data.length == 0)
                    {
                        this.prev();
                    }
                }
                else this.end = false;
            });
        }

        prev()
        {
            this.page--;

            this.getData();
        }

        next()
        {
            this.page++;

            this.getData();
        }
    }

    ng.controller("userList", UserListController);
}