namespace RouteApp 
{
    let ng = angular.module("RouteApp").lazy;

    export class LoginPageController implements angular.IController
    {
        public form = {
            user: '',
            pass: ''
        };

        constructor(private authSrv: RouteApp.IAuthService, private scope: angular.IScope, private loc: angular.ILocationService)
        {
            console.log("Status Login : ", authSrv.isLogedIn());
        }

        public getForm()
        {
            return this.scope['form'] as angular.IFormController;
        }

        public doLogin()
        {   
            if (this.getForm().$invalid) return;
            // console.log(this.form);
            return this.authSrv.login(this.form.user,this.form.pass)
            .then(d => {
                alert("User Loggedin");
                console.log("logged in")
                // Can see add after login
                this.loc.path("/");
                return d;
            });
        }
    }

    LoginPageController.$inject = ['AuthService', '$scope', '$location'];

    ng.controller("loginPage", LoginPageController);
}