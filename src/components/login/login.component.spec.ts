// @see 
// declare jasmine, ugh.. 
// angular-mocks only available for jasmine or mocha.. zzzz
// Old tech.. sad...
// This can be seen form the angular-mocks.js in node_modules, This is my own workaround
(window.jasmine as any) = jest;

// require is like, well.. 

require("angular");
require("angular-route");
require("angular-cookies");
require('angular-mocks/ngMockE2E');
require("jest");

// Other Controller Dependency of component
require('../../config');
require('../../services/auth.service');
require('../botak-text/botak-text.component');
let prov = angular.module("RouteApp", ['BotakTextModule', 'AuthServiceModule']);
// Workaround to lazy load for specific... welp... God Please help.. 
// haha... 
(prov as any).lazy = {
    controller: prov.controller,
    provider: prov.provider,
    factory: prov.factory,
    service: prov.service,
    config: prov.config,
    module: prov
}
// load the component
require('./login.component');
let htmlTemplate = require('./login.component.html');

describe("LoginPageController", () => {

    let component: RouteApp.LoginPageController;
    const module = angular.mock.module;
    const inject = angular.mock.inject;
    let q: angular.IQService;
    let rootScope: angular.IRootScopeService;

    var templateHtml, formElem, form, srv;

    var ctrl: angular.IControllerService, scope: angular.IRootScopeService|any;

    beforeEach(() => {
        module('RouteApp', 'AuthServiceModule', "ngMockE2E");
        // To inject the controller and other, we need to do these things... 
        // $scope = https://stackoverflow.com/a/43274428/4906348
        // $controller = https://stackoverflow.com/a/59054213/4906348
        inject(function($rootScope: angular.IRootScopeService, $controller: angular.IControllerService, $httpBackend: angular.IHttpBackendService, 
                $compile: angular.ICompileService, $q: angular.IQService) {
            // console.log(JSON.parse(_$controller_));
            // console.log($httpBackend);
            // Allow the E2E connect to BE/Mock API
            $httpBackend.whenPOST(/\/*/).passThrough();
            $httpBackend.whenGET(/\/*/).passThrough();
            ctrl = $controller;
            rootScope = $rootScope;
            q = $q;
            scope = $rootScope.$new();
            templateHtml = htmlTemplate;
            formElem = angular.element(`${templateHtml}`);
            $compile(formElem)(scope)
            form = scope.form;

            // We need to use inject here, so the digest will work
            // If not the E2E won't even work, and this cost me a lot of time to debug
            // Using karma and babel isn't option, but using JEST and AngularJS is sad.. 
            srv = angular.injector(['AuthServiceModule']).get("AuthService");
            scope.$apply()
            // console.log("huh",templateHtml);
        });

        // We need manually provide the srv... sad.. 
        component = ctrl('loginPage', {$scope: scope, AuthService: srv});
    });

    // This for focus test case, for focus fdescribe
    it("created class component", () => {
        expect(component).toBeDefined();
    });

    // This only mocks because jest-puppeteer hasn't installed
    it("must able to login", async () => {
        component.form.user = 'ben';
        component.form.pass = 'ben';
        // let defer = q.defer<any>();
        
        // defer.resolve({msg: "hai"})
        // console.log(form);

        // let promise = defer.promise;
        
        const spyOnLoginThen = jest.spyOn(component, "doLogin"); //.mockReturnValue(promise);
        
        component.getForm().$invalid = false; // Sad.. 
        // console.log(component.getForm().$invalid);

        // This is for mock. but when e2e test using jest
        // Still don't know.. sadly.. 
        // @see https://stackoverflow.com/a/16082272/4906348
        // scope.$apply();
        // For diggest, read more @see https://docs.angularjs.org/guide/unit-testing#testing-promises
        rootScope.$digest();

        return component.doLogin().then(d => {
            console.log(d.data);
            expect((d as any).id).not.toBeNull();
            expect(spyOnLoginThen).toHaveBeenCalled();
            console.log("Promise is working.. ");

            return d;
        });
    });

    it("must not sent data when invalid", () => {
        const spyOnLoginThen = jest.spyOn(component, "doLogin"); //.mockReturnValue(promise);
        // console.log(component.getForm().$invalid);
        component.getForm().$invalid = true;

        let res = component.doLogin();
        expect(res).toBeUndefined();
        expect(component.getForm().$invalid).toBeTruthy();
        expect(spyOnLoginThen).toHaveBeenCalled();
    });

    it("module and inject is not null", () => {
        expect(module).not.toBeNull();
        expect(inject).not.toBeNull();
    });
});