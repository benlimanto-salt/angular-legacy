/// <reference path="../node_modules/@types/angular/index.d.ts" />
/// <reference path="../node_modules/@types/angular-route/index.d.ts" />
/// <reference path="interfaces/auth.interface.ts" />
namespace RouteApp
{

    const ng = angular.module("RouteApp", ['ng', 'ngRoute', 'BotakTextModule', 'AuthServiceModule']);

    // Other way to return the same module with lazyLoad properties... 
    angular.module = (name, require, configFn) => {
        // console.log("Terpanggil " + name);
        if (name == "RouteApp") return ng;
        return angular.module(name, require, configFn);
    };

    // Only enable this if the client browser is HTML5 support
    // IE9/10 is not part of it
    ng.config(['$locationProvider', function($locationProvider) {
        $locationProvider.html5Mode(true);
    }]);

    class RouteAppController implements angular.IOnInit {
        static $inject = ['lazyService', '$scope', '$location', 'AuthService', '$http'];
        constructor(srv: LazyService, private scope: angular.IScope, private loc: angular.ILocationService,
            public authSrv: RouteApp.IAuthService, private http: angular.IHttpService)
        {
            // console.log(srv);
        }
        
        /**
         * This method is called because it's not lazy loaded, so no need to 
         * add it into constructor
         */
        $onInit(): void {
            console.log("onit");
            this.MapAuthMiddleware();
            this.http.get(`${config.apiURL}/asdasd`).then(d => {
                console.log(d);
            });

            let q: angular.IQService;
            var p = q.defer();
            p.promise.then(d => {
                console.log(d);
            }).catch(c => {console.log("No CATCH : ", c)});
            setTimeout(() => {
                p.resolve({"msg": "resolved"});
            }, 1500);
        }

        /**
         * Clever Idea to handle ngRoute
         * based on the riptuts, but changed into OOP using Typescript
         * @see https://riptutorial.com/angularjs/example/22736/defining-custom-behavior-for-individual-routes#example
         */
        public MapAuthMiddleware()
        {
            this.scope.$on('$routeChangeStart', (angularEvent: angular.IAngularEvent, newUrl?: angular.route.IRoute) => {
                console.log(angularEvent);
                
                if (newUrl == null) {
                    // workaround to get new path after changed... 
                    setTimeout(() => {throw new Error(`This route (${window.location.href}) doesn't exist!`)}, 500)
                    return;
                };

                // console.log(angularEvent);
                let userRole = this.authSrv.roles();
                let count = 0;
                let routeRole: Array<any> = newUrl.roles ?? [];

                routeRole.forEach(r => {
                    if (userRole.filter(f => f == r).length > 0) count++;
                });

                if (count < userRole.length && routeRole.length > 0)
                {
                    console.log("Route : ", newUrl.roles);
                    // User isn’t allowed to access the route
                    this.loc.path("/");
                }

                if (newUrl.requireAuth && !this.authSrv.isLogedIn()) {
                    // User isn’t authenticated
                    this.loc.path("/");
                }
            });
        }
        
        public click()
        {
            alert("Test");
        }

        public login()
        {
            this.loc.path("/login")
        }

        public isLoggedIn()
        {
            return this.authSrv.isLogedIn();
        }

        public user()
        {
            return this.authSrv.getUser();
        }

        public logout()
        {
            this.authSrv.logout().then(d => {
                alert("User logged out");
            });
        }
    }
    
    function RouterConfig(route: angular.route.IRouteProvider, ctrl: angular.IControllerProvider, prov: angular.auto.IProvideService, lz: LazyService)
    {
        // console.log("LZ : ", lz);
        // Monkey sad... using Monkey DI
        lz = lz.$get();

        // This force adding/caching the old before bootstraped angular ... :/ 
        // @see https://stackoverflow.com/a/32061320/4906348
        (ng as any).lazy = {
            controller: ctrl.register,
            provider: prov.provider,
            factory: prov.factory,
            service: prov.service,
            config: ng.config,
            module: ng
        }

        route.when("/", lz.resolve("taskList", "task-list", ['tasks']))
        .when("/login", lz.resolve("loginPage", "login"))
        .when("/task/add", lz.resolve("taskAdd", "task-add", ['tasks'], {requiredAuth: true}))
        .when("/task/:id", lz.resolve("taskEdit", "task-edit", ['tasks'], {requiredAuth: true}))
        .when("/user", lz.resolve("userList", "user-list", ['user'], {requiredAuth: true, roles: ['admin']}, 'users/user-list'))
        .when("/user/:id", lz.resolve("taskEdit", "task-edit", ['user'], {requiredAuth: true, roles: ['admin']}));
    }

    RouterConfig.$inject = ['$routeProvider', '$controllerProvider', '$provide', 'lazyServiceProvider'];

    ng.controller("RouteController", RouteAppController);

    ng.config(RouterConfig);
}