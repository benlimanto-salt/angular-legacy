namespace App 
{
    'use strict'

    export const config = {
        templatePath: 'assets/js/components'
    };

    let ng = angular.module('App', ['AppRouteModule']);

    class AppController {
        constructor()
        {

        }

        click() 
        {
            alert("Angular 1");
        }

        goToRoute()
        {
            window.location.pathname = "/route/";
        }
    }

    class ListController {
        public text: string = "IniText";

        public data: string[] = ["a", "b", "c", "d", "e"];


    }

    ng.controller("AppController", AppController)
        .controller("ListController", ListController);
}