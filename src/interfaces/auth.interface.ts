declare namespace angular.route
{
    interface IRoute {
        /**
         * This follow the principle of Using Auth
         * Quite clever moves, so this interface help us to overwrite the contract of IRoute in @types/angular-route
         * @see https://riptutorial.com/angularjs/example/22736/defining-custom-behavior-for-individual-routes
         */
        requireAuth?: boolean;

        /**
         * This is my own implementation using .NET Core Roles approach
         */
        roles: string[];
    }
}